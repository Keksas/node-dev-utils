# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## Added
## Removed

## [1.2.2] - 2017-10-11

### Added
- proxyquire
- unit tests

### Updated
- bin scripts to support windows

## [1.2.0] - 2017-07-03

### Added
- Bootstrapping for database mocks

## [1.1.1] - 2017-06-11

### Added
- Ability to extend bootstrap file

## [1.1.0] - 2017-06-10

### Added
- Instructions on how to setup tests
- Sinon for spies, stubs and mocks

### Removed
- Installation script

## [1.0.0] - 2017-04-29

### Added
- Documentation generator
- Testing framework and coverage report generator
