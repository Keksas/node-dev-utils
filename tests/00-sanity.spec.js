describe('node-dev-tools', () => {
  it('is sane', () => {});

  it('runs on correct environment', () => {
    expect(process.env.NODE_ENV).to.equal('test');
  });

  it('works with promises', () => {
    const promise = Promise.resolve({
      foo: 'bar',
      biz: [{ one: 1 }, { two: 2 }]
    });

    return Promise.all([
      expect(promise).to.eventually.be.fulfilled,
      expect(promise).to.eventually.have.property('foo', 'bar'),
      expect(promise).to.eventually.have.nested.property('biz[0].one', 1)
    ]);
  });
});
