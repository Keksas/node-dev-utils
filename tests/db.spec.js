const DB = require('../src/db');

const models = {
  user: db => db.sequelize.define('user', {
    name: db.Sequelize.DataTypes.STRING
  })
};

let testModels;

describe('Database mocking', () => {
  before(() => {
    testModels = DB.initModels(models);
  });

  it('seeds the models', () => {
    const action = DB.reset(testModels, {
      user: [{ name: 'Jon' }]
    });

    return expect(action).to.be.fulfilled;
  });

  it('finds the user', () => {
    expect(testModels.user.find({ where: { name: 'Jon' } }))
      .to.eventually.have.property('name', 'Jon');
  });
});
