Provides quick way to write tests using [mocha](https://mochajs.org/) test framework.

This package provides following npm binaries:
- node-docs
- node-test

They're all are placed in `node_modules/.bin/` so you can run them from there. Or you can modify your `package.json` and use `npm run <script>`
```
"scripts": {
    "test": "node-test",
    "docs": "node-docs"
},
```

## API Documentation

Auto generated API documentation can be found [here](docs/api.md).

## Unit tests

Command `node-test`

This package provides bootstrap for testing. It's up to the developer to decide on test directory structure. By default `node-test` will recursively read `tests/` directory and run tests defined in `*.spec.js` files.

Unit tests are written using expect style. [Style Guide](http://chaijs.com/guide/styles/#expect)

#### Tools
- [mocha](https://mochajs.org/) test framework
- [chai](http://chaijs.com)([api](http://chaijs.com/api/bdd/)) assertion library
- [chai-as-promised](https://github.com/domenic/chai-as-promised) for promise testing
- [sinon](http://sinonjs.org/) spies, stubs and mocks
- [istanbuljs/nyc](https://github.com/istanbuljs/nyc) covertage reporting

Most of the tools are bootstrapped in `src/bootstrap-tests.js`. It exposes following global variables used in every test:
- `expect`
- `sinon`

#### Test coverage reporting

This command also outputs tests coverage report. It can be found in `docs/test-coverage/index.html`. Running the tests will output a short summary of coverage report to stdout and regenerate the report.

Example coverage report displayed after running the test:

```
-------------------|----------|----------|----------|----------|----------------|
File               |  % Stmts | % Branch |  % Funcs |  % Lines |Uncovered Lines |
-------------------|----------|----------|----------|----------|----------------|
All files          |    74.51 |     37.5 |    73.33 |    76.09 |                |
 src               |      100 |      100 |      100 |      100 |                |
  relationships.js |      100 |      100 |      100 |      100 |                |
 src/controllers   |       75 |     37.5 |    77.78 |    73.17 |                |
  root.js          |      100 |      100 |      100 |      100 |                |
  user.js          |    73.17 |     37.5 |       76 |    71.79 |... 64,66,78,83 |
 src/routes        |       50 |      100 |        0 |      100 |                |
  root.js          |       50 |      100 |        0 |      100 |                |
  user.js          |       50 |      100 |        0 |      100 |                |
-------------------|----------|----------|----------|----------|----------------|
```

#### Usage

`node-test [SOURCE_DIRECTORY] [TEST_PATTERN] [REPORT_DIRECTORY]`

SOURCE_DIRECTORY (Default: `src/`) is used for coverage report. This should point to a directory where your source code is placed.

TEST_PATTERN (Default: `tests/**/*.spec.js`) is used to find test files.

REPORT_DIRECTORY (Default: `docs/test-coverage/`) is used to define where to place generated coverage report.

#### Database mocks

This package provides database data mocking through [sequelizejs](sequelizejs.com). To create the mocks you need to provide an array of sequelizejs models and optionally a seed object. This will create an in memory sqlite instance, call [sync()](http://docs.sequelizejs.com/class/lib/sequelize.js~Sequelize.html#instance-method-sync) and insert seed data.

#### Linter

There are linter rules tailored for `*.spec.js`. See `gitlab:Keksas/eslint-config-wolf`.

```
echo '{"extends": "wolf/tests"}' > tests/.eslintrc
```

## Documentation generator

Command `node-docs`

It uses [jsdoc-to-markdown](https://www.npmjs.com/package/jsdoc-to-markdown). So all you have to do is write valid jsdoc annotations and run `node-docs`. The command is setup to output to `docs/api.md` and expects source files to be located at `src/`

