<a name="module_node-dev-utils/dbMock"></a>

## node-dev-utils/dbMock

* [node-dev-utils/dbMock](#module_node-dev-utils/dbMock)
    * _static_
        * [.initModels(models, [storage])](#module_node-dev-utils/dbMock.initModels) ⇒ <code>Object</code>
        * [.reset(models, seeds)](#module_node-dev-utils/dbMock.reset) ⇒ <code>Promise.&lt;T&gt;</code>
    * _inner_
        * [~syncModels(models)](#module_node-dev-utils/dbMock..syncModels) ⇒ <code>Promise.&lt;T&gt;</code>
        * [~seedModels(models, seeds)](#module_node-dev-utils/dbMock..seedModels) ⇒ <code>Promise.&lt;T&gt;</code>

<a name="module_node-dev-utils/dbMock.initModels"></a>

### node-dev-utils/dbMock.initModels(models, [storage]) ⇒ <code>Object</code>
Prepares sqlite connection and initializes models with it.
If you need to inspect mocked database state, provide storage file system path.
Eg: storage = path.resolve(os.tmpdir(), `app-test-data.sqlite`)

**Kind**: static method of [<code>node-dev-utils/dbMock</code>](#module_node-dev-utils/dbMock)  
**Returns**: <code>Object</code> - - initialized models  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| models | <code>Object</code> |  | Models to be mocked |
| [storage] | <code>string</code> | <code>&quot;:memory:&quot;</code> | Defines how to store data. Defaults to memory |

<a name="module_node-dev-utils/dbMock.reset"></a>

### node-dev-utils/dbMock.reset(models, seeds) ⇒ <code>Promise.&lt;T&gt;</code>
Recreates and seeds tables

**Kind**: static method of [<code>node-dev-utils/dbMock</code>](#module_node-dev-utils/dbMock)  
**Returns**: <code>Promise.&lt;T&gt;</code> - -  

| Param | Type | Description |
| --- | --- | --- |
| models | <code>Object</code> | Object containing initialized models |
| seeds | <code>Object</code> | Corresponding seeds |

<a name="module_node-dev-utils/dbMock..syncModels"></a>

### node-dev-utils/dbMock~syncModels(models) ⇒ <code>Promise.&lt;T&gt;</code>
Recreates tables for models

**Kind**: inner method of [<code>node-dev-utils/dbMock</code>](#module_node-dev-utils/dbMock)  
**Returns**: <code>Promise.&lt;T&gt;</code> - -  

| Param | Type | Description |
| --- | --- | --- |
| models | <code>Object</code> | Object containing initialized models |

<a name="module_node-dev-utils/dbMock..seedModels"></a>

### node-dev-utils/dbMock~seedModels(models, seeds) ⇒ <code>Promise.&lt;T&gt;</code>
Seeds the models

**Kind**: inner method of [<code>node-dev-utils/dbMock</code>](#module_node-dev-utils/dbMock)  
**Returns**: <code>Promise.&lt;T&gt;</code> - -  

| Param | Type | Description |
| --- | --- | --- |
| models | <code>Object</code> | Object containing initialized models |
| seeds | <code>Object</code> | Corresponding seeds |

