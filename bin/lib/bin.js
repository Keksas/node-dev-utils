const path = require('path');
const os = require('os');

/**
 * Resolve path to npm binary
 * takin inot consideration os being used
 * @param  {string} name Binary file name
 * @return {string} Resolved path to binary
 */
module.exports = (name) => {
  const isWindows = os.platform() === 'win32';
  const binName = isWindows ? `${name}.cmd` : name;
  return path.join('node_modules', '.bin', binName);
};
