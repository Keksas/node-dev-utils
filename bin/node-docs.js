#!/usr/bin/env node

const path = require('path');
const exec = require('./lib/exec');
const bin = require('./lib/bin');


/**
 * Specifies where source files are placed
 * @type {string}
 */
const SOURCE_FILES = path.join('src', '*.js');

/**
 * Specifies where generated docs are placed
 * @type {string}
 */
const TARGET_FILE = path.join('docs', 'api.md');

const args = [
  '--plugin',
  'dmd-bitbucket',
  '--files',
  SOURCE_FILES
];

exec(bin('jsdoc2md'), args, TARGET_FILE);
