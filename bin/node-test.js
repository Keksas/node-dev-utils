#!/usr/bin/env node

const path = require('path');
const exec = require('./lib/exec');
const bin = require('./lib/bin');

/**
 * Defines where to look for JavaScript source files
 * @type {string}
 */
const SOURCE_DIRECTORY = 'src/';

/**
 * Defines where to find tests
 * @type {string}
 */
const TEST_PATTERN = path.join('tests', '**', '*.spec.js');

/**
 * Defines where test coverage report will be output
 * @type {string}
 */
const REPORT_DIR = path.join('docs', 'coverage');

/**
 * This file will be used as bootstrap for tests
 * @type {string}
 */
const BOOTSTRAP = path.resolve(__dirname, '..', 'src', 'bootstrap');

const args = process.argv.slice(2, process.argv.length);
const noReport = args.indexOf('no-report');

if (noReport !== -1) {
  args.splice(noReport, 1);
  exec(bin('mocha'), [
    '--recursive',
    TEST_PATTERN,
    '--require',
    BOOTSTRAP,
    ...args
  ]);
} else {
  exec(bin('nyc'), [
    '--reporter=html',
    '--reporter=text',
    `--report-dir=${REPORT_DIR}`,
    `--include=${SOURCE_DIRECTORY}`,
    '--exclude',
    '**/models/**',
    '--exclude',
    '**/migrations/**',
    bin('mocha'),
    '--recursive',
    TEST_PATTERN,
    '--require',
    BOOTSTRAP,
    ...args
  ]);
}
