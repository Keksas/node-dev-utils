const fs = require('fs');
const path = require('path');

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const proxyquire = require('proxyquire');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.use(chaiAsPromised);
chai.use(sinonChai);

process.env.NODE_ENV = 'test';

global.expect = chai.expect;
global.sinon = sinon;
global.proxyquire = proxyquire;

// If file tests/bootstrap.js exist include it
// This will allow application to have its custom bootstrap code placed there
const bootstrap = path.join(process.cwd(), 'tests', 'bootstrap.js');
if (fs.existsSync(bootstrap)) {
  require(bootstrap); // eslint-disable-line global-require,import/no-dynamic-require
}
