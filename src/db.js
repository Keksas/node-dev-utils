/**
 * @module node-dev-utils/dbMock
 */

const Sequelize = require('sequelize');

/**
 * Recreates tables for models
 * @param  {Object} models - Object containing initialized models
 * @return {Promise<T>} -
 */
const syncModels = (models) => {
  const promises = [];
  Object.keys(models).forEach((key) => {
    promises.push(models[key].sync({ force: true }));
  });
  return Promise.all(promises);
};

/**
   * Seeds the models
   * @param  {Object} models Object containing initialized models
   * @param  {Object} seeds  Corresponding seeds
   * @return {Promise<T>}-
   */
const seedModels = (models, seeds) => {
  const promises = [];
  Object.keys(seeds).forEach(key => seeds[key].forEach(seed =>
    promises.push(models[key].create(seed)))
  );
  return Promise.all(promises);
};

module.exports = {
  /**
   * Prepares sqlite connection and initializes models with it.
   * If you need to inspect mocked database state, provide storage file system path.
   * Eg: storage = path.resolve(os.tmpdir(), `app-test-data.sqlite`)
   * @param {Object} models             - Models to be mocked
   * @param {string} [storage=:memory:] - Defines how to store data. Defaults to memory
   * @return {Object}                   - initialized models
   */
  initModels: (models, storage = ':memory:') => {
    const sequelize = new Sequelize({
      logging: false,
      dialect: 'sqlite',
      operatorsAliases: {},
      storage
    });

    const nextModels = {};
    Object.keys(models).forEach((key) => {
      nextModels[key] = models[key]({ Sequelize, sequelize });
    });

    return nextModels;
  },

  /**
   * Recreates and seeds tables
   * @param  {Object} models - Object containing initialized models
   * @param  {Object} seeds  - Corresponding seeds
   * @return {Promise<T>}-
   */
  reset: (models, seeds) => syncModels(models)
    .then(() => seedModels(models, seeds))
};

